/* global oComponent_MyProfile : true */
sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/Device"
], 
    /**
     * provide app-view type models (as in the first "V" in MVVC)
     * 
     * @param {typeof sap.ui.model.json.JSONModel} JSONModel
     * @param {typeof sap.ui.Device} Device
     * 
     * @returns {Function} createDeviceModel() for providing runtime info for the device the UI5 app is running on
     */
    function (JSONModel, Device) {
        "use strict";

        return {
            createDeviceModel: function () {
                var oModel = new JSONModel(Device);
                oModel.setDefaultBindingMode("OneWay");
                return oModel;
        },
        createJSONModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
                
            });
			oModel.setSizeLimit(1000);
			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		},
        createGeneralModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
                showSignCanvas: true,
				showSignImage: false,
				afterSave: false,
                showPassword: false
            });
			oModel.setSizeLimit(1000);
			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		},
        LoadPharmData: function (sUser) {
			debugger;
			const sKey = oComponent_MyProfile.getModel("ODATA").createKey("/GetPharmDataSet", {
				IvUname: sUser
			});
			return new Promise(function (resolve, reject) {
				oComponent_MyProfile.getModel("ODATA").read(sKey, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);

						console.log(error);

					}
				});
			});
		},
		UpdatePharmData: function(oEntry){
            return new Promise(function(resolve, reject) {
				oComponent_MyProfile.getModel("ODATA").create("/UpdatePharmDataSet", oEntry,{
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	

		}
    };
});
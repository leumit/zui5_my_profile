/* global oComponent_MyProfile: true */
sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "zui5_my_profile/model/models",
    "sap/m/MessageBox",
    'sap/m/MessageToast'
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, models, MessageBox, MessageToast) {
        "use strict";

        return Controller.extend("zui5_my_profile.controller.Main", {
            onInit: function () {
                oComponent_MyProfile._MainController = this;
                this.getPharmData();


            },
            getPharmData: function () {
                oComponent_MyProfile.getModel("ODATA").metadataLoaded().then(() => {
                    var sUser = ""; //027511187
                    models.LoadPharmData(sUser).then((data) => {
                        oComponent_MyProfile.getModel("JSON").setData(jQuery.extend({}, data));


                    }).catch((error) => {
                        models.handleErrors(error);
                    });
                }).catch((error) => {
                    models.handleErrors(error);
                });

            },
            onOpenDialog: function (oEvent, sDialogName) {

                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment("zui5_my_profile.view.popovers." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                this[sDialogName].open();
            },
            onOpenPopover: function (oEvent, sName) {
                if (!this[sName]) {
                    this[sName] = sap.ui.xmlfragment("zui5_my_profile.view.popovers." + sName + 'Popover', this);
                    this.getView().addDependent(this[sName]);
                }
                this[sName].openBy(oEvent.getSource());
            },
            onCloseDialog: function (oEvent, sDialogName) {

                try {
                    this[sDialogName].close();
                } catch (e) {
                    oComponent_MyProfile[sDialogName].close();
                }

            },
            beforeOpenSignDialog: function () {
                var oData = oComponent_MyProfile.getModel("JSON").getProperty("/Signature");

            },
            ClearSign: function () {
                var canvas = document.getElementById("signCanv");
                // oComponent_MyProfile.getModel("JSON").setProperty("/SignContent", "");
                oComponent_MyProfile.getModel("JSON").setProperty("/SignImage", "");
                oComponent_MyProfile.getModel("General").setProperty("/path", "");
                oComponent_MyProfile.getModel("General").setProperty("/SignImage", "");
                oComponent_MyProfile.getModel("General").setProperty("/showSignCanvas", true);
                oComponent_MyProfile.getModel("General").setProperty("/showSignImage", false);
                canvas.width = canvas.width;
            },
            StartSign: function () {
                function SetValues(objResponse, imageWidth, imageHeight) {
                    var obj = null;
                    if (typeof (objResponse) === 'string') {
                        obj = JSON.parse(objResponse);
                    } else {
                        obj = JSON.parse(JSON.stringify(objResponse));
                    }

                    var ctx = document.getElementById('cnv').getContext('2d');

                    // if (obj.errorMsg !== null && obj.errorMsg != "" && obj.errorMsg != "undefined") {
                    // 	alert(obj.errorMsg);
                    // } else {
                    if (obj.isSigned) {
                        // document.FORM1.sigRawData.value += obj.imageData;
                        // document.FORM1.sigStringData.value += obj.sigString;
                        var img = new Image();
                        img.onload = function () {
                            ctx.drawImage(img, 0, 0, imageWidth, imageHeight);
                        }
                        img.src = "data:image/png;base64," + obj.imageData;
                        oComponent_CD.getModel("JSON").setProperty("/SignData", obj);
                    }
                    // }
                }

                function SignResponse(event) {
                    var str = event.target.getAttribute("msgAttribute");
                    var obj = JSON.parse(str);
                    if (!!obj.errorMsg) {
                        oComponent_MyProfile._MainController.mouseSign();
                    } else {
                        SetValues(obj, imgWidth, imgHeight);
                    }

                }

                var isInstalled = document.documentElement.getAttribute('SigPlusExtLiteExtension-installed');
                if (!isInstalled) {
                    oComponent_MyProfile._MainController.mouseSign();
                    return;
                }
                var canvasObj = document.getElementById('signCanv');

                // canvasObj.getContext('2d').clearRect(0, 0, canvasObj.width, canvasObj.height);

                // document.FORM1.sigStringData.value = "SigString: ";
                // document.FORM1.sigRawData.value = "Base64 String: ";
                var imgWidth = canvasObj.width;
                var imgHeight = canvasObj.height;
                var message = {
                    "firstName": "",
                    "lastName": "",
                    "eMail": "",
                    "location": "",
                    "imageFormat": 1,
                    "imageX": imgWidth,
                    "imageY": imgHeight,
                    "imageTransparency": false,
                    "imageScaling": false,
                    "maxUpScalePercent": 0.0,
                    "rawDataFormat": "ENC",
                    "minSigPoints": 25
                };

                top.document.addEventListener('SignResponse', SignResponse, false);
                var messageData = JSON.stringify(message);
                var element = document.createElement("MyExtensionDataElement");
                element.setAttribute("messageAttribute", messageData);
                document.documentElement.appendChild(element);
                var evt = document.createEvent("Events");
                evt.initEvent("SignStartEvent", true, false);
                element.dispatchEvent(evt);

                function ClearFormData() {
                    document.FORM1.sigStringData.value = "SigString: ";
                    document.FORM1.sigRawData.value = "Base64 String: ";
                    document.getElementById('SignBtn').disabled = false;
                }
            },
            mouseSign: function () {
                debugger;
                window.requestAnimFrame = (function (callback) {
                    return window.requestAnimationFrame ||
                        window.webkitRequestAnimationFrame ||
                        window.mozRequestAnimationFrame ||
                        window.oRequestAnimationFrame ||
                        window.msRequestAnimaitonFrame ||
                        function (callback) {
                            window.setTimeout(callback, 1000 / 60);
                        };
                })();

                var canvas = document.getElementById("signCanv");
                var ctx = canvas.getContext("2d");
                ctx.strokeStyle = "#222222";
                ctx.lineWidth = 4;

                var drawing = false;
                var mousePos = {
                    x: 0,
                    y: 0
                };
                var lastPos = mousePos;

                canvas.addEventListener("mousedown", function (e) {
                    drawing = true;
                    lastPos = getMousePos(canvas, e);
                }, false);

                canvas.addEventListener("mouseup", function (e) {
                    drawing = false;
                }, false);

                canvas.addEventListener("mousemove", function (e) {
                    mousePos = getMousePos(canvas, e);
                }, false);

                // Add touch event support for mobile
                canvas.addEventListener("touchstart", function (e) {

                }, false);

                canvas.addEventListener("touchmove", function (e) {
                    var touch = e.touches[0];
                    var me = new MouseEvent("mousemove", {
                        clientX: touch.clientX,
                        clientY: touch.clientY
                    });
                    canvas.dispatchEvent(me);
                }, false);

                canvas.addEventListener("touchstart", function (e) {
                    mousePos = getTouchPos(canvas, e);
                    var touch = e.touches[0];
                    var me = new MouseEvent("mousedown", {
                        clientX: touch.clientX,
                        clientY: touch.clientY
                    });
                    canvas.dispatchEvent(me);
                }, false);

                canvas.addEventListener("touchend", function (e) {
                    var me = new MouseEvent("mouseup", {});
                    canvas.dispatchEvent(me);
                }, false);

                function getMousePos(canvasDom, mouseEvent) {
                    var rect = canvasDom.getBoundingClientRect();
                    return {
                        x: mouseEvent.clientX - rect.left,
                        y: mouseEvent.clientY - rect.top
                    }
                }

                function getTouchPos(canvasDom, touchEvent) {
                    var rect = canvasDom.getBoundingClientRect();
                    return {
                        x: touchEvent.touches[0].clientX - rect.left,
                        y: touchEvent.touches[0].clientY - rect.top
                    }
                }

                function renderCanvas() {
                    if (drawing) {
                        ctx.moveTo(lastPos.x, lastPos.y);
                        ctx.lineTo(mousePos.x, mousePos.y);
                        ctx.stroke();
                        lastPos = mousePos;
                    }
                }

                // Prevent scrolling when touching the canvas
                document.body.addEventListener("touchstart", function (e) {
                    if (e.target == canvas) {
                        e.preventDefault();
                    }
                }, false);
                document.body.addEventListener("touchend", function (e) {
                    if (e.target == canvas) {
                        e.preventDefault();
                    }
                }, false);
                document.body.addEventListener("touchmove", function (e) {
                    if (e.target == canvas) {
                        e.preventDefault();
                    }
                }, false);

                (function drawLoop() {
                    requestAnimFrame(drawLoop);
                    renderCanvas();
                })();

                function clearCanvas() {
                    canvas.width = canvas.width;
                }



            },
            SaveSign: function () {
                debugger;
                var canvas = document.getElementById("signCanv");
                var isNotEmpty = canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height).data
                    .some(channel => channel !== 0);
                if (isNotEmpty) {
                    var dataUrl = canvas.toDataURL();
                    var b64file = dataUrl.substring(dataUrl.indexOf("base64,") + "base64,".length);
                    oComponent_MyProfile.getModel("JSON").setProperty("/Signature", b64file);
                    var SignDetails = { File: { lastModifiedDate: new Date(), type: "image/png" } };
                    oComponent_MyProfile.getModel("JSON").setProperty("/SignDetails", SignDetails);
                    // this.addFiles(b64file, SignDetails, 'sign');
                    this.onCloseDialog('', 'Sign');
                    oComponent_MyProfile.getModel("JSON").setProperty("/Signature", b64file);
                }
                else {
                    this.onCloseDialog('', 'Sign');
                    oComponent_MyProfile.getModel("JSON").setProperty("/Signature", '');
                }
                // var sigText.innerHTML = dataUrl;
                // var sigImage.setAttribute("src", dataUrl);
            },
            showPassword: function (oEvent) {
                var bShow = oComponent_MyProfile.getModel("General").getProperty("/showPassword");
                oComponent_MyProfile.getModel("General").setProperty("/showPassword", !bShow);
            },
            onEditPassword: function (oEvent) {
                debugger
                var sValue = oEvent.getParameter("newValue");
                if (sValue.length < 5) {
                    return;
                }
                else {
                    oEvent.getSource().setValue(sValue.substr(0, 4));
                }
            },
            isBase64UrlImage: function (base64String) {
                debugger;
                let image = new Image();
                image.src = 'data:image/png;base64,' + base64String;
                return new Promise(function (resolve, reject) {
                    image.onload = function () {
                        if (image.height === 0 || image.width === 0) {
                            resolve(false);
                        }
                        resolve(true)
                    }
                    image.onerror = () => {
                        resolve(false)
                    }
                });
            },
            onSavePharm: function (oEvent) {
                var oUserData = oComponent_MyProfile.getModel("JSON").getData();
                // this.isBase64UrlImage(oUserData.Signature).then((isBase64) => {
                //     debugger;
                // }).catch((error) => {
                //     models.handleErrors(error);
                // });
                if (!oUserData.Password || !oUserData.Signature || oUserData.Password.length < 4 || !Number(oUserData.Password)) {
                    var msg = !oUserData.Password ? oComponent_MyProfile.i18n('passwordMsg') : oUserData.Password.length < 4 ? oComponent_MyProfile.i18n('passwordErrorMsg') : !Number(oUserData.Password)  ? oComponent_MyProfile.i18n('passwordLengthErrorMsg') : oComponent_MyProfile.i18n('signErrorMsg');
                    oComponent_MyProfile.getModel("General").setProperty("/afterSave", true);
                    MessageToast.show(msg, {
                        duration: 1500
                    });
                    return;
                }
                var oEntry = {
                    IvPassword: oUserData.Password,
                    IvSignature: oUserData.Signature,
                    Return: []
                }
                models.UpdatePharmData(oEntry).then(function (data) {
                    MessageToast.show(oComponent_MyProfile.i18n('updateMsg'), {
                        duration: 1500
                    });
                    oComponent_MyProfile.getModel("General").setProperty("/afterSave", false);
                    this.getPharmData();
                }.bind(this)).catch(function (error) {
                    console.log(error);
                    try {
                        MessageBox.error(JSON.parse(error.responseText).error.message.value);
                    } catch (e) {
                        MessageBox.error(JSON.stringify(error));
                    }
                });
            }
        });
    });

/* global QUnit */

sap.ui.require(["zui5_my_profile/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});

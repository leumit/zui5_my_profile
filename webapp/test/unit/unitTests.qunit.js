/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"zui5_my_profile/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});

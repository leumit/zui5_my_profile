/**
 * eslint-disable @sap/ui5-jsdocs/no-jsdoc
 */
var oComponent_MyProfile;
sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "zui5_my_profile/model/models"
],
    function (UIComponent, Device, models) {
        "use strict";

        return UIComponent.extend("zui5_my_profile.Component", {
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: function () {
                oComponent_MyProfile = this;
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);
                //Set Right to Left
                sap.ui.getCore().getConfiguration().setRTL(true);
                //Set HE language
                sap.ui.getCore().getConfiguration().setLanguage("iw_IL");

                // enable routing
                this.getRouter().initialize();

                // set the device model
                this.setModel(models.createDeviceModel(), "device");
                this.setModel(models.createJSONModel(), "JSON");
                this.setModel(models.createGeneralModel(), "General");
                this.getModel("ODATA").attachBatchRequestSent(function () {
                    sap.ui.core.BusyIndicator.show();
                }).attachBatchRequestCompleted(function (event) {
                    sap.ui.core.BusyIndicator.hide();
                });
            },
            i18n: function (str) {
                return oComponent_MyProfile.getModel("i18n").getProperty(str);
            }
        });
    }
);